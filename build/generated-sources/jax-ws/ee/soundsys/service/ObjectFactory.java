
package ee.soundsys.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.soundsys.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetProductResponse_QNAME = new QName("http://www.soundsys.ee/service", "getProductResponse");
    private final static QName _AddProductResponse_QNAME = new QName("http://www.soundsys.ee/service", "addProductResponse");
    private final static QName _RentProductsResponse_QNAME = new QName("http://www.soundsys.ee/service", "rentProductsResponse");
    private final static QName _GetProductsResponse_QNAME = new QName("http://www.soundsys.ee/service", "getProductsResponse");
    private final static QName _GetInvoiceResponse_QNAME = new QName("http://www.soundsys.ee/service", "getInvoiceResponse");
    private final static QName _GetInvoicesResponse_QNAME = new QName("http://www.soundsys.ee/service", "getInvoicesResponse");
    private final static QName _DeleteProductResponse_QNAME = new QName("http://www.soundsys.ee/service", "deleteProductResponse");
    private final static QName _PayInvoiceResponse_QNAME = new QName("http://www.soundsys.ee/service", "payInvoiceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.soundsys.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetProductRequest }
     * 
     */
    public GetProductRequest createGetProductRequest() {
        return new GetProductRequest();
    }

    /**
     * Create an instance of {@link ProductType }
     * 
     */
    public ProductType createProductType() {
        return new ProductType();
    }

    /**
     * Create an instance of {@link AddProductRequest }
     * 
     */
    public AddProductRequest createAddProductRequest() {
        return new AddProductRequest();
    }

    /**
     * Create an instance of {@link OrderType }
     * 
     */
    public OrderType createOrderType() {
        return new OrderType();
    }

    /**
     * Create an instance of {@link OrderListType }
     * 
     */
    public OrderListType createOrderListType() {
        return new OrderListType();
    }

    /**
     * Create an instance of {@link RentProductsRequest }
     * 
     */
    public RentProductsRequest createRentProductsRequest() {
        return new RentProductsRequest();
    }

    /**
     * Create an instance of {@link InvoiceType }
     * 
     */
    public InvoiceType createInvoiceType() {
        return new InvoiceType();
    }

    /**
     * Create an instance of {@link GetProductsRequest }
     * 
     */
    public GetProductsRequest createGetProductsRequest() {
        return new GetProductsRequest();
    }

    /**
     * Create an instance of {@link ProductListType }
     * 
     */
    public ProductListType createProductListType() {
        return new ProductListType();
    }

    /**
     * Create an instance of {@link GetInvoiceRequest }
     * 
     */
    public GetInvoiceRequest createGetInvoiceRequest() {
        return new GetInvoiceRequest();
    }

    /**
     * Create an instance of {@link GetInvoicesRequest }
     * 
     */
    public GetInvoicesRequest createGetInvoicesRequest() {
        return new GetInvoicesRequest();
    }

    /**
     * Create an instance of {@link InvoiceListType }
     * 
     */
    public InvoiceListType createInvoiceListType() {
        return new InvoiceListType();
    }

    /**
     * Create an instance of {@link DeleteProductRequest }
     * 
     */
    public DeleteProductRequest createDeleteProductRequest() {
        return new DeleteProductRequest();
    }

    /**
     * Create an instance of {@link PayinvoiceRequest }
     * 
     */
    public PayinvoiceRequest createPayinvoiceRequest() {
        return new PayinvoiceRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "getProductResponse")
    public JAXBElement<ProductType> createGetProductResponse(ProductType value) {
        return new JAXBElement<ProductType>(_GetProductResponse_QNAME, ProductType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "addProductResponse")
    public JAXBElement<OrderListType> createAddProductResponse(OrderListType value) {
        return new JAXBElement<OrderListType>(_AddProductResponse_QNAME, OrderListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "rentProductsResponse")
    public JAXBElement<InvoiceType> createRentProductsResponse(InvoiceType value) {
        return new JAXBElement<InvoiceType>(_RentProductsResponse_QNAME, InvoiceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "getProductsResponse")
    public JAXBElement<ProductListType> createGetProductsResponse(ProductListType value) {
        return new JAXBElement<ProductListType>(_GetProductsResponse_QNAME, ProductListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "getInvoiceResponse")
    public JAXBElement<InvoiceType> createGetInvoiceResponse(InvoiceType value) {
        return new JAXBElement<InvoiceType>(_GetInvoiceResponse_QNAME, InvoiceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "getInvoicesResponse")
    public JAXBElement<InvoiceListType> createGetInvoicesResponse(InvoiceListType value) {
        return new JAXBElement<InvoiceListType>(_GetInvoicesResponse_QNAME, InvoiceListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "deleteProductResponse")
    public JAXBElement<OrderListType> createDeleteProductResponse(OrderListType value) {
        return new JAXBElement<OrderListType>(_DeleteProductResponse_QNAME, OrderListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.soundsys.ee/service", name = "payInvoiceResponse")
    public JAXBElement<InvoiceType> createPayInvoiceResponse(InvoiceType value) {
        return new JAXBElement<InvoiceType>(_PayInvoiceResponse_QNAME, InvoiceType.class, null, value);
    }

}
