/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SoundSysPackage.Resources;

import SoundSysPackage.ProductsStorage;
import SoundSysPackage.TokenValidation;
import ee.soundsys.service.*;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author kasutaja
 */
@Path("products")
public class ProductsResource {

    

    /**
     * Creates a new instance of ProductsResource
     */
    public ProductsResource() {
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ProductListType getProducts(@QueryParam("token") String token,
                                        @QueryParam("invoiceId") int invoiceId,
                                        @QueryParam("rating") int rating,
                                        @QueryParam("ceiling") double ceiling
                                        ) {
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){   
            return ProductsStorage.getProducts(
                    customerId, invoiceId, rating, ceiling);
        }       
        return null;
    }
    
    @GET
    @Path("/{productId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductType getProduct(@PathParam("productId") int productId,
                                    @QueryParam("token") String token){
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){   
            return ProductsStorage.getProductById(productId);
        }       
        return null;
    }
}
