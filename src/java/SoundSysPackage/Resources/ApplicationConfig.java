/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SoundSysPackage.Resources;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author kasutaja
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        try{
            Class jsonProvider = Class.forName("org.glassfish.jersey.jackson.JacksonFeature");
            resources.add(jsonProvider);
        }
        catch(ClassNotFoundException ex){
            System.out.println(ex);
        }
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(SoundSysPackage.Resources.InvoicesResource.class);
        resources.add(SoundSysPackage.Resources.OrdersResource.class);
        resources.add(SoundSysPackage.Resources.ProductsResource.class);
    }
    
}
