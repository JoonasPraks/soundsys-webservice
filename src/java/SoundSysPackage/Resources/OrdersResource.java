package SoundSysPackage.Resources;

import SoundSysPackage.InvoiceStorage;
import SoundSysPackage.OrdersStorage;
import SoundSysPackage.ProductsStorage;
import SoundSysPackage.TokenValidation;
import ee.soundsys.service.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;

@Path("orders")
public class OrdersResource {

    public OrdersResource() {
    }
  
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OrderListType addProduct(AddProductRequest body, @QueryParam("token") String token) {
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){
            if(ProductsStorage.getProductById(body.getOrder().getProductId())!= null ){
                return OrdersStorage.addToList(customerId, body.getOrder());
            }
        }       
        return null;
    }
    
    @POST
    @Path("/invoices/")
    @Produces(MediaType.APPLICATION_JSON)
    public InvoiceType rentProducts(@QueryParam("token") String token,
                                    @QueryParam("days") int days) 
                                    throws DatatypeConfigurationException {
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){
            OrderListType orderList = OrdersStorage.getRentProducts(customerId);
            if(orderList!=null && days>0){
                if(InvoiceStorage.isLastPaid(customerId)){
                    if(ProductsStorage.removeRented(orderList)){
                        orderList.setAmountOfDays(days);
                        InvoiceType inv = InvoiceStorage.addInvoice(customerId, OrdersStorage.getRentProducts(customerId));
                        OrdersStorage.removeRentProducts(customerId);
                        return inv;
                    }
                }
            }
        }
        return null;
    }
    
    @DELETE
    @Path("/{productId}/")
    @Produces(MediaType.APPLICATION_JSON)
    public OrderListType deleteProduct(@PathParam("productId") int productId,
                                        @QueryParam("token") String token
                                                ) {
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){
            return OrdersStorage.deleteProduct(customerId, productId);
        }       
        return null;
    }
}
