/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SoundSysPackage.Resources;

import SoundSysPackage.InvoiceStorage;
import SoundSysPackage.TokenValidation;
import ee.soundsys.service.*;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@Path("invoices")
public class InvoicesResource {
    
    public InvoicesResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public InvoiceListType getInvoices(@QueryParam("token") String token,
                                        @QueryParam("after") Date after,
                                        @QueryParam("before") Date before,
                                        @QueryParam("ceiling") double ceiling
                                        ) throws DatatypeConfigurationException  {
        
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){
            GregorianCalendar c = new GregorianCalendar();
            XMLGregorianCalendar afterGreg = null;
            XMLGregorianCalendar beforeGreg = null;
            if(after!=null){
                c.setTime(after);
                afterGreg = DatatypeFactory.newInstance().newXMLGregorianCalendar(c); 
            }
            if(before!=null){
                c.setTime(before);
                beforeGreg = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            }
            return InvoiceStorage.getInvoices( customerId,
                        afterGreg, beforeGreg, ceiling);
            }
        return null;
    
    }

    @GET
    @Path("/{invoiceId}")
    @Produces(MediaType.APPLICATION_JSON)
    public InvoiceType getInvoice(@PathParam("invoiceId") int invoiceId,
                                    @QueryParam("token") String token){
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0 && invoiceId > 0){
            return InvoiceStorage.getInvoice(customerId, invoiceId);
        }
        return null;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public InvoiceType payInvoice(@QueryParam("token") String token) {
        int customerId = TokenValidation.Validate(token);
        if (customerId > 0){
            return InvoiceStorage.setInvoicePaid(customerId);
        }       
        return null;
    }
}
