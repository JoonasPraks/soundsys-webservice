package SoundSysPackage;

import ee.soundsys.service.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class InvoiceStorage {
    /**
     * Generates a new invoice for the customer based on the products
     * in his orderList
     * @param customerId Id of the customer
     * @param orderList List of orders associated with the customer
     * @return New Invoice
     * @throws DatatypeConfigurationException 
     */
    public static InvoiceType addInvoice(int customerId, OrderListType orderList) 
            throws DatatypeConfigurationException{
        InvoiceType invoice = new InvoiceType();
        InvoiceListType invList = invoiceDict.get(customerId);
        if(invList==null){
            invList = new InvoiceListType();
            invoice.setInvoiceId(1);
        }
        else{
            List<InvoiceType> invListList = invoiceDict.get(customerId).getInvoice();
            invoice.setInvoiceId(invListList.get(invListList.size()-1).getInvoiceId()+1);
        }
        
        Date now = new java.util.Date();
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(now);
        invoice.setInvoiceDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        
        int dueDays = orderList.getAmountOfDays();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);            
        calendar.add(Calendar.DAY_OF_YEAR, dueDays);
        c.setTime(calendar.getTime());
        invoice.setDueDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        
        int listSize = orderList.getOrder().size();
        double price = 0;
        double pricePerDay = 0;
        int amount = 0;
        for(int i = 0; i<listSize; i++){
            pricePerDay = ProductsStorage.getProductById(orderList.getOrder().get(i).getProductId()).getPricePerDay().doubleValue();
            amount = orderList.getOrder().get(i).getQuantity();
            price += amount*pricePerDay*orderList.getAmountOfDays();
        }       
        invoice.setPrice(BigDecimal.valueOf(price));
        
        invoice.setIsPaid(false);
        
        invoice.setOrderList(orderList);
        
        invList.getInvoice().add(invoice);    
        invoiceDict.put(customerId, invList);
                
        return invoice;
    }
    /**
     * Finds an invoice with the corresponding id
     * @param customerId Id of the customer
     * @param invoiceId Id associated with the requested invoice
     * @return Specified invoice
     */
    public static InvoiceType getInvoice(int customerId, int invoiceId){
        if (invoiceId>0){
            List<InvoiceType> list = invoiceDict.get(customerId)!=null?
                    invoiceDict.get(customerId).getInvoice():null;
            if(list!=null){
                for(int i = 0; i<list.size(); i++){
                    if(list.get(i).getInvoiceId()==invoiceId)
                        return list.get(i);
                }
            }
        }
        return null;
    }
    
    /**
     * Finds the specified invoices
     * @param customerId Id of the customer
     * @param afterDate Optional parameter to set the minimum date of invoices
     * @param beforeDate Optional parameter to set the maximum date of invoices
     * @param ceiling Optional parameter to set the maximum value of invoices
     * @return Specified invoices
     */
    public static InvoiceListType getInvoices(int customerId, 
            XMLGregorianCalendar afterDate, XMLGregorianCalendar beforeDate, 
                                                            double ceiling){
            InvoiceListType invList = invoiceDict.get(customerId);
            List<InvoiceType> list = invList!=null ? invList.getInvoice():null;
            if(list!=null){
                InvoiceListType filteredInvoiceList = new InvoiceListType();
                list.stream().filter((invoice) -> 
                        ((afterDate == null || invoice.getInvoiceDate().compare(afterDate) == DatatypeConstants.GREATER)
                    && (beforeDate == null || invoice.getDueDate().compare(beforeDate) == DatatypeConstants.LESSER))
                        &&(ceiling == 0.0 || invoice.getPrice().doubleValue() <= ceiling))
                        .forEachOrdered((invoice) -> 
                    {
                        filteredInvoiceList.getInvoice().add(invoice);
                    });
                return filteredInvoiceList;
            }
        return null;
    }
    
    /**
     * Checks whether the last invoice is paid for. If no invoice exists, 
     * returns true
     * @param customerId Id of the customer
     * @return Boolean on whether the payment has been done
     */
    public static boolean isLastPaid(int customerId){
        if(!invoiceDict.containsKey(customerId))
            return true;
        List<InvoiceType> invList = invoiceDict.get(customerId).getInvoice();
        return invList.get(invList.size()-1).isIsPaid();
    }
    
    /**
     * Labels the last invoice as being paid, 
     * thus permitting the addition of a new invoice
     * @param customerId Id of the customer
     * @return Last invoice
     */
    public static InvoiceType setInvoicePaid(int customerId){
        if(!invoiceDict.containsKey(customerId))
            return null;
        List<InvoiceType> invList = invoiceDict.get(customerId).getInvoice();
        invList.get(invList.size()-1).setIsPaid(true);
        return invList.get(invList.size()-1);
    }
    
   public static Map<Integer, InvoiceListType> invoiceDict = new HashMap<Integer, InvoiceListType>();
}
