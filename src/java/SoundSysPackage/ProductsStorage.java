
package SoundSysPackage;

import ee.soundsys.service.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductsStorage {
    
    /**
     * Finds a product with the corresponding id
     * @param productId Id of the product
     * @return Specified product
     */
    public static ProductType getProductById(int productId){
        for (ProductType product : productList){
            if (product.getProductId() == productId){
                return product;
                }           
        }
        return null;
    }
    
    /**
     * Finds the specified products
     * @param customerId Id of the customer
     * @param invoiceId Optional parameter to get products tied with 
     * the specified invoice
     * @param rating Optional parameter to set the rating of products
     * @param ceiling Optional parameter to set the maximum price of products
     * @return List of specified products
     */
    public static ProductListType getProducts(int customerId, int invoiceId, int rating, double ceiling){
        
        List<ProductType> prodList = null;
        List<OrderType> orderList = null;
        if(invoiceId!=0){
                prodList = new ArrayList<>();
                InvoiceType inv = InvoiceStorage.getInvoice(customerId, invoiceId);
                if(inv!=null){
                    orderList = inv.getOrderList().getOrder();
                    for(OrderType order : orderList){
                        prodList.add(getProductById(order.getProductId()));
                    }
                }
            }
        else{
            prodList = productList;
        }
        ProductListType filteredProductList = new ProductListType();
              
        /*return new ProductListType(productList.stream().filter(prod -> rating != 0 && prod.getRating() == rating)
                .filter(prod -> priceCeiling != 0 && prod.getPricePerDay() <= priceCeiling)
                .collect(Collectors.toList()));*/
        
        prodList.stream().filter((product) -> ((rating == 0 || product.getRating() == rating)
                && (ceiling == 0.0 || product.getPricePerDay().doubleValue() <= ceiling))).forEachOrdered((product) -> 
                {
                    filteredProductList.getProduct().add(product);
        });
        return filteredProductList;
    }
    
    /**
     * Checks whether there are enough products to match the quantities in
     * orderList and if true removes an amount of products 
     * from the product storage.
     * @param orderList List of orders whose corresponding products are 
     * to be removed.
     * @return Boolean on whether the products where successfully removed.
     */
    public static boolean removeRented(OrderListType orderList){
        int listSize = orderList.getOrder().size();
        boolean allAvailable = true;
        int availableProds = 0;
        for(int i = 0; i<listSize; i++)
        {
            OrderType order = orderList.getOrder().get(i);
            for (ProductType product : productList){
                if (order.getProductId() == product.getProductId()){
                    if(order.getQuantity() > product.getInStock())
                        allAvailable = false;                    
                    else
                        availableProds++;
                    break;
                }     
            }
        }
        if(allAvailable && availableProds>0){
            for(int i = 0; i<listSize; i++)
            {
                OrderType order = orderList.getOrder().get(i);
                for (ProductType product : productList){                  
                    if (order.getProductId() == product.getProductId()){
                        if(order.getQuantity() <= product.getInStock())
                            product.setInStock(product.getInStock()-order.getQuantity());
                        break;
                        }
                }
            }
            return true;
        }
        return false;
    }
    
    public static ProductType product1 = new ProductType();
    static{
        product1.setProductId(278);
        product1.setType("speaker");
        product1.setMake("TS-D6902R");
        product1.setCompany("Pioneer");
        product1.setRating(4);
        product1.setPricePerDay(BigDecimal.valueOf(25));
        product1.setInStock(20);
    }
    
    public static ProductType product2 = new ProductType();
    static{
        product2.setProductId(576);
        product2.setType("speaker");
        product2.setMake("CBR15");
        product2.setCompany("Yamaha");
        product2.setRating(5);
        product2.setPricePerDay(BigDecimal.valueOf(20));
        product2.setInStock(30);
    }
    
    public static ProductType product3 = new ProductType();
    static{
        product3.setProductId(345);
        product3.setType("speaker");
        product3.setMake("Flip");
        product3.setCompany("JBL");
        product3.setRating(3);
        product3.setPricePerDay(BigDecimal.valueOf(5));
        product3.setInStock(5);
    }
    public static ProductType product4 = new ProductType();
    static{
        product4.setProductId(679);
        product4.setType("microphone");
        product4.setMake("FV-100");
        product4.setCompany("Sony");
        product4.setRating(4);
        product4.setPricePerDay(BigDecimal.valueOf(25));
        product4.setInStock(10);
    }
    public static List<ProductType> productList = new ArrayList<>();
    static{
        productList.add(product1);
        productList.add(product2);
        productList.add(product3);
        productList.add(product4);
    }
}
