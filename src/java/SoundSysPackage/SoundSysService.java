package SoundSysPackage;

import com.sun.xml.ws.developer.SchemaValidation;
import ee.soundsys.service.*;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;

@WebService(serviceName = "SoundSysService", portName = "SoundSysPort", endpointInterface = "ee.soundsys.service.SoundSysPortType", targetNamespace = "http://www.soundsys.ee/service", wsdlLocation = "WEB-INF/wsdl/SoundSysService/SoundSysService.wsdl")
@SchemaValidation
public class SoundSysService {

    public ProductType getProduct(GetProductRequest parameter) {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            return ProductsStorage.getProductById((parameter.getId()));
        }    
        return null;
    }

    public OrderListType addProduct(AddProductRequest parameter) {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            if(ProductsStorage.getProductById(parameter.getOrder().getProductId())!= null ){
                return OrdersStorage.addToList(customerId, parameter.getOrder());
            }
        }       
        return null;
    }   

    public InvoiceType rentProducts(RentProductsRequest parameter) throws DatatypeConfigurationException {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            OrderListType orderList = OrdersStorage.getRentProducts(customerId);
            if(orderList!=null && parameter.getAmountOfDays()>0){
                if(InvoiceStorage.isLastPaid(customerId)){
                    if(ProductsStorage.removeRented(orderList)){
                        orderList.setAmountOfDays(parameter.getAmountOfDays());
                        InvoiceType inv = InvoiceStorage.addInvoice(customerId, OrdersStorage.getRentProducts(customerId));
                        OrdersStorage.removeRentProducts(customerId);
                        return inv;
                    }
                }
            }
        }
        return null;
    }

    public ProductListType getProducts(GetProductsRequest parameter) {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            double ceiling;
            if (parameter.getHasLowerPriceThan()==null){
                ceiling = 0;
            }
            else{
                ceiling = parameter.getHasLowerPriceThan().doubleValue();
            }          
            return ProductsStorage.getProducts(
                    customerId, parameter.getInvoiceId(), parameter.getHasWhichRating(), ceiling);
        }       
        return null;
    }

    public InvoiceType getInvoice(GetInvoiceRequest parameter) {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            return InvoiceStorage.getInvoice(customerId, parameter.getInvoiceId());
        }
        return null;
    }

    public InvoiceListType getInvoices(GetInvoicesRequest parameter) {      
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            double ceiling;
            if (parameter.getHasLowerValueThan()==null){
                ceiling = 0;
            }
            else{
                ceiling = parameter.getHasLowerValueThan().doubleValue();
            }          
            return InvoiceStorage.getInvoices( customerId,
                    parameter.getAfterDate(), parameter.getBeforeDate(), 
                    ceiling);
        }
        return null;   
    }
    
    public OrderListType deleteProduct(DeleteProductRequest parameter){
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            return OrdersStorage.deleteProduct(customerId, parameter.getProductId());
        }       
        return null;
    }

    public InvoiceType payInvoice(PayinvoiceRequest parameter) {
        int customerId = TokenValidation.Validate(parameter.getToken());
        if (customerId > 0){
            return InvoiceStorage.setInvoicePaid(customerId);
        }       
        return null;
    }   
}
