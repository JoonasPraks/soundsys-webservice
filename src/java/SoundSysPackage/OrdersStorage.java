/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SoundSysPackage;

import ee.soundsys.service.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author kasutaja
 */


public class OrdersStorage {
    
    /**
     * Adds an order to customers order storage
     * @param customerId Id of the customer
     * @param order Order to be added
     * @return Updated list of orders
     */
    public static OrderListType addToList(int customerId, OrderType order){
        OrderListType orderList = OrderDict.get(customerId);
        
        if(orderList!=null){
            int listSize = orderList.getOrder().size();
            for(int i = 0; i<listSize; i++)
            {
                if(orderList.getOrder().get(i).getProductId()==order.getProductId()){
                    return orderList;
                }
            }
            orderList.getOrder().add(order);
        }
        else{
            orderList = new OrderListType();
            orderList.getOrder().add(order);
        }
        //Uuendab ProductRentDicti uue rentlistiga
        OrderDict.put(customerId, orderList);
        return orderList;
    }
    
    public static OrderListType getRentProducts(int customerId){
        return OrderDict.get(customerId);
    }
    public static void removeRentProducts(int customerId){
        OrderDict.remove(customerId);
    }
    
    /**
     * Deletes a specified order from order storage
     * @param customerId Id of the customer
     * @param productId Id of the product whose order is to be deleted
     * @return 
     */
    public static OrderListType deleteProduct(int customerId, int productId){
        //Üritab kätte saada kasutaja käesolevat rendilisti
        OrderListType orderList = OrderDict.get(customerId);
        
        //Kui olemas, siis lisab productRenti vanale listile
        if(orderList!=null){
            int listSize = orderList.getOrder().size();
            for(int i = 0; i<listSize; i++)
            {
                //Kui säärane toode juba nimekirjas, siis ei luba enam lisada
                if(orderList.getOrder().get(i).getProductId()==productId){
                    orderList.getOrder().remove(i);
                    break;
                }
            }
        }
        return orderList;
    }
    
    public static Map<Integer, OrderListType> OrderDict = new HashMap<Integer, OrderListType>();
}
